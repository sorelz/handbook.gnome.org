# GNOME Handbook

This site uses [Sphinx](https://www.sphinx-doc.org/en/master/) to generate a [static website](https://handbook.gnome.org/), which is deployed using [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/). It is primarily written in [reStructuredText](https://docutils.sourceforge.io/docs/user/rst/quickref.html).

## Goals

The goal of the this handbook is to provide all the knowledge someone might need to participate in the GNOME project. Much of this involves writing down information that existing contributors take for granted.

Other goals include:

* Present content in a way that is accessible to those who are new to GNOME.
 * Only cover general project-level GNOME information:
   * Don't document how to use the GNOME platform - that's using GNOME technologies (as opposed to participating in GNOME itself).
   * Don't document team information, since it's not project-level.
* Keep the content maintainable and easy to use, by limiting its scope and level of detail.

## Contributions

The GNOME Handbook is a collective resource, and contributions are highly encouraged. The primary contribution method is through the [standard merge request workflow](https://handbook.gnome.org/development/change-submission.html). However, we want to make things easy for potential contributors:

* If you'd like commit access, just ask.
* For small changes feel free to submit text suggestions as issues, and someone will commit them for you.
* Rough and ready change proposals are welcome - they don't have to be super polished or correctly formatted.

Notes are available on [how to use reStructured Text](https://gitlab.gnome.org/Teams/Websites/handbook.gnome.org/-/blob/main/rst-primer.rst).

## Building locally

If you are making bigger changes, it is a good idea to build the site locally, since the Sphinx build tool checks for and warns about any errors, which can be very useful.

**1. Install dependencies**

On Fedora, run:

```
$ dnf install -y python3-pip
$ pip3 install --upgrade --user furo sphinx sphinxext-opengraph pip-tools
```

**2. Build**

From the project root, run: `./00localbuild.sh`.The build output can then be found in `/build`.

You can host the site with python, for testing:
```
cd build && python3 -m http.server
```

## Updating dependencies

To update dependencies (you need to have pip-tools installed), run:

```sh
pip-compile
```
