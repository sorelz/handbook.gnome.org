Legal
=====

This page details GNOME's legal guidelines and requirements for development projects.

Licensing
---------

As an open source project, GNOME requires that all its modules be licensed using an OSI approved license (see the `SPDX license list <https://spdx.org/licenses/>`_ for reference). This includes repositories of assets and resources, as well as code.

The primary licenses used in GNOME are:

* GNU General Public License, versions 2 and 3
* GNU Lesser Public License, version 2
* Creative Commons Attribution Sharealike (CCBYSA)

Each module's license should be included in its ``COPYING`` file.

Copyright Notices
-----------------

While they are not strictly necessary, it is recommended that source code files include a copyright notice.

Some guidelines:

* Existing copyright notices should not be removed
* Avoid copyright notices for individual contributors, since this can lead to maintenance overhead
* For new files, the following copyright notice is recommended: ``Copyright the <Module Name> authors``

For more information, see `this article <https://www.linuxfoundation.org/blog/blog/copyright-notices-in-open-source-software-projects>`_.

Copyright Assignment
--------------------

The GNOME project has a `policy of rejecting copyright assignment <https://wiki.gnome.org/FoundationBoard/Resources/CopyrightAssignment>`_.
