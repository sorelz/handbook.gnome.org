Building With Toolbx
====================

This page provides a tutorial for those who want to build GNOME modules using `Toolbx <https://containertoolbx.org/>`_ for the build environment. (For an overview of the build methods available for GNOME, see :doc:`building </development/building>`.)

The tutorial below uses a Fedora-based Toolbx container, but most commands will work with other distros. It also assumes some preexisting familiarity with Git version control.

Introduction to Toolbx
----------------------

Each Toolbx container is a like a separate operating system which shares some things with your host operating system (like the files in your home directory) but whose software is kept separate, so you can install packages and manually build modules into it, without interfering with your host operating system.

Toolbx containers are typically used through the terminal. Once created, you enter a Toolbx container in the terminal and from that point, most commands are carried out by the container rather than the host operating system.

While "inside" a Toolbx container environment, you can access files from your home directory. However, other parts of the file system come from the container and are not shared with the host operating system.

To use Toolbx, it needs to be installed on your system. If you are running Fedora Workstation or Fedora Silverblue, it is preinstalled. On other distributions you will need to manually install it.

Creating a Toolbx container
---------------------------

To create a container with Toolbx, run::

    $ toolbox create gnome

Here, ``gnome`` is the name that will be gvien to the new container.

By default, the container is based on the latest version of the host operating system. So, if you are using Fedora, then the new container will use the latest stable Fedora release. (If you want to specify the distro and version to be used, you can use the ``--distro`` and ``--version`` options.) When building GNOME modules, it is beneficial to use a distro that contains a recent GNOME version.

If this is the first time a container has been created with Toolbx, it will download an image from which to create the container. This is a large file so may take a while. However, once the image has been downloaded, creating containers from it is quick.

Once the Toolbx container has been created, enter it using::

    $ toolbox enter gnome

You will see that the prompt is now preceded by an ⬢ symbol. This indicates that the prompt is inside a Toolbx container.

Building modules
----------------

Once you have a Toolbx container, you can build GNOME modules inside it. The following instructions for this are generic should work for most GNOME modules. However, you should check a module's ``README`` file before trying to build it.

First, create a directory in which to keep the source code of the modules that you want to build::

    ⬢ mkdir checkout && cd checkout

Then, use Git to clone the repository of the module you want to build. For example, to clone gnome-clocks you would run::

    ⬢ git clone https://gitlab.gnome.org/GNOME/gnome-clocks.git

Once the repository has been cloned, switch to it and install any required dependencies. For Fedora containers, an easy way to do this is using ``dnf builddep``::

    ⬢ cd gnome-clocks
    ⬢ sudo dnf builddep -y gnome-clocks

Now configure the build::

    ⬢ meson setup builddir --prefix=/usr

This means that the build will be cached in the directory called ``builddir``, which is inside the project directory, and that the built version of the module will be put in the ``/usr`` directory.

  ``/usr`` is inside the container and not the host operating system, so building and installing to this location will not affect your system. However, this is also the location where software is kept on the system, so building to this location will overwrite an existing version of the module you are building. This also means that using the container's package manager to update or reinstall the module will overwrite the version that you have manually built. You are essentially replacing parts of the containers system software with the code you are building.

Build the module::

    ⬢ meson compile -C builddir

Install the module::

    ⬢ sudo meson install -C builddir

Finally, stop any instances of the module which might already be running, and then run the newly installed version::

    ⬢ killall gnome-clocks && gnome-clocks

Update and make changes
-----------------------

Assuming your build was successful, you can now make changes to the code, rebuild the module, and run it with your changes. To update the repository to get new changes, run::
    
    ⬢ git pull -r

To build the project with any changes, rerun::

    ⬢ meson compile -C builddir

To run the rebuilt project, rerun::

    ⬢ killall gnome-clocks && gnome-clocks

When things go wrong
--------------------

You may encounter issues when manually building GNOME modules. Typically these are easy to resolve and just require a bit of practice and detective work.

Missing dependencies
~~~~~~~~~~~~~~~~~~~~

Sometimes, dependencies required by a module may not be installed or may not be new enough. If this happens, the module will fail to build.

If the build has failed, first check the build output in the terminal for error messages. There should be a message which indicates which module is missing or of the wrong version, which may look like ``Config error: No package 'foo-2.0' found``.

Next, try to install the missing package using the package manager. This may require a bit of detective work to figure out which package is needed. You can try searching in your package manager for the missing module. In Fedora containers, you can also use dnf to install a particular pkg-config file that is missing. For example::
    
    ⬢ sudo dnf install 'pkgconfig(foo-2.0)'

When installing dependencies, you will often need to install the version of the package, which is suffixed with ``-devel`` or ``-dev``.

If the required module isn't available as a package, or the available package isn't new enough, you will need to manually download and install it, using the manual build method described above.

If you aren't able to find the package or GNOME module that is missing, then you can :doc:`ask for advice on Matrix </get-in-touch/matrix>`.

Changes not being built
~~~~~~~~~~~~~~~~~~~~~~~

If you have made changes to a module, and rebuilding doesn't give you those changes, it could be due to a caching issue. To resolve this, try running ``meson compile`` with the ``--clean`` option. You can also delete the ``builddir`` directory from the project folder and then recompile the project.
