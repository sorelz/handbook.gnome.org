Commit Messages
===============

Commit messages provide context to changes that a diff cannot convey by itself. They help other people understand changes, and document the reasons behind changes when coming back to them after months or years.

This page provides general recommendations and guidelines that apply to most GNOME modules. Some modules have different or additional conventions, so it is recommended to check for project-specific documentation and look at existing commit messages before contributing.

A more extensive documentation in line with the GNOME commit style including reasoning is available at `the coala project <https://api.coala.io/en/latest/Developers/Writing_Good_Commits.html>`_.

Structure
---------

Commit messages use the following structure:

#. A brief description that summarizes the commit
#. The main description (body)
#. Links or references to related issues (optional)

The different parts are separated by blank lines (two newlines).

Dedicated tools like the `Commit app <https://apps.gnome.org/en/Commit/>`_ can help with the formatting.

Example
-------

.. code:: text

    tag: Short explanation of the commit

    Longer explanation explaining exactly what's changed and why, whether
    any external or private interfaces changed, what issue were fixed (with
    issue number if applicable) and so forth. Be concise but not too brief.

    Closes #1234

Summary line
------------
* A summary of the change itself, not the issue of effect/beneifts (that goes in the description).
* Always a single sentence with no trailing period (full stop).
* Be sure to not exceed 72 characters; you can use '&' instead of 'and' so the message gets shorter.
* Uses imperative present tense. For example use "Add Hindi translation" instead of "Added Hindi translation".
* You can prefix the first line with one tag, to make it easier to know to which part of the module the commit applies. For example, a commit with "sound: Fix label alignments" in the `Settings module <https://gitlab.gnome.org/GNOME/gnome-control-center>`_ clearly applies to the sound panel.

Main description
----------------
* A detailed description of the change and the reasoning behind it. Consider the commit message as an email sent to the developers (or yourself, six months in the future) detailing **why** you changed something. There's no need to specify the **how**: those who need to know can view the code changes alongside the commit message.
* Each line must not exceed 75 characters. There is no limit on the number of lines.
* Uses normal prose, punctuation and capitalization.
* If you use ``git revert`` command to undo a commit, git generates a default commit message specifying the hash and commit message of the commit you're undoing, but this is not enough to know *why* you're reverting it, so please add more comments to it.

References
----------
* If your commit is addressing an issue, use the `GitLab syntax <https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically>`_ to automatically close the issue when merging the commit with the upstream repository::

    Closes #1234
    Fixes #1234
    Closes: https://gitlab.gnome.org/GNOME/gtk/issues/1234

* When committing code on behalf of others use the ``--author`` option and add a Signed-off-by line with ``--signoff``::

    git commit -a --author "Joe Coder <joe@coder.org>" --signoff
