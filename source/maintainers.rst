Maintainers
===========

This page describes what being a maintainer of a GNOME module involves. It is intended to serve as a guide for new maintainers, as well as a reference for existing maintainers.

Expectations for modules
------------------------

Maintainers are expected to ensure that their modules are properly maintained. Here, "properly maintained" means:

* New releases are created in accordance with the `release schedule <https://release.gnome.org/calendar/index.html>`_ (see :doc:`making a release </maintainers/making-a-release>`).
* Each stable release has a dedicated stable branch (see :doc:`branching </maintainers/branches>`).
* MRs are reviewed in a timely fashion.
* Issue reports are responded to, and the number of open issues is under control (see the :doc:`issue management </issues/management>` for guidance on this).
* Security issues are reported to the security team by creating a `new issue with the security team <https://gitlab.gnome.org/Teams/Releng/security/-/issues/new>`_.
* Documentation is up to date, and the module has a contribution and build guide.
* New contributors are encouraged and existing contributors are guided into positions of responsibility.
* The latest platform developments are followed, such as using the latest build tooling and library versions.
* The code of conduct is followed.

While it is the job of maintainers to make sure that all of this happens, they don't have to do all the work themselves: ideally, much of it will be done by other contributors.

Maintainer expectations
-----------------------

In addition to making sure that their module(s) are properly maintained, maintainers should be subscribed to the `announcement <https://discourse.gnome.org/tag/announcement>`_ and `maintainers <https://discourse.gnome.org/tag/maintainers>`_ tags in Discourse (see :ref:`subscribing via email`). They should also be subscribed to any module-specific tags for their projects, should they exist.

Maintainers should also also be aware of everything that is happening in their modules. Watching a project in GitLab is a good way to do this.

Maintainer powers
-----------------

Maintainers have a number of exclusive abilities:

* They are the only ones who can create and upload new releases (though in some situations the Release Team may help with this).
* Only maintainers can assign new maintainers to their modules.
* They get the final say over which code changes are merged.

Becoming a maintainer
---------------------

Maintainer status is typically granted by a module's existing maintainer. If a module doesn't have a maintainer, anyone who takes on the maintenance role by doing the required work then becomes the maintainer.

If there is an existing maintainer for the module, once it has been agreed that you are a new maintainer, you should:

* Add yourself to the module's ``.doap`` file. If the module is part of the GNOME GitLab group, this assigns you the maintainer role in GitLab and is needed for master.gnome.org access.
* Request :ref:`master.gnome.org access <master-access>`, if you don't have it already.

.. toctree::
   :hidden:

   maintainers/making-a-release
   maintainers/branches
