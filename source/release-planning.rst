Release Planning
================

This page provides information about GNOME's release processes. Release planning is the responsibility of the :doc:`Release Team <release-planning/release-team>`, which maintains this page.

The Release Cycle
-----------------

The GNOME Project uses a time-based release cycle, with a new stable version being released every six months, every March and September. What each six-month release cycle looks like:

* For most of the cycle, development proceeds unencumbered, with developers working on enhancements and new features, and code changes being merged.
* As the end of the cycle approaches, three unstable development versions are released. These are scheduled in advance and are used for testing. They are included in the unstable releases of some Linux distributions.
* Also towards the end of the cycle, a series of :doc:`freezes </release-planning/freezes>` come into effect. These limit the scope of the changes that can be made so that, it can often be more difficult to have code changes merged while the freezes are in effect (since most modules don't create stable branches until after the GNOME stable release).
* At the end of the cycle, a new stable version is released and this is what is eventually used by distributions. The new version is accompanied by `release notes <https://release.gnome.org/>`_, which describe the changes it contains.
* A development cycle is generally considered to be over once a new stable version has been released. However, the stable release doesn't mean that work on the new version completely stops and, typically, each stable version is updated with a small number of stable point releases.

The `schedule for the current development cycle <https://release.gnome.org/calendar/index.html>`_ includes the dates of upcoming development versions, freezes, and stable versions.

Branches
--------

GNOME modules can vary in how their branches are organized. However, development typically happens on main branches, and main branches are typically open for changes to be merged for the majority of each cycle. Once a stable version is released, each module creates a new stable branch for that version (see :doc:`branching </maintainers/branches>`), to allow the release of stable update releases while active development continues on the main branch.

Versioning
----------

Stable GNOME releases have a version number which is a whole integer. For example: ``40.0``, ``41.0``, ``42.0``. Stable update releases use the same integer and increase the decimal point. For example: ``40.1``, ``40.2``, ``41.1``, ``41.2``.

Unstable development releases have the same version number as the stable release of that development cycle, with either an ``alpha``, ``beta``, or ``rc`` (release candidate) suffix. For example: ``40.alpha``, ``40.beta``, ``40.rc``.

This versioning schema was first introduced for GNOME 40. Previous GNOME releases used even point versions for stable releases (for example: ``3.12``, ``3.14``, ``3.16``) and odd point versions for unstable releases (for example: ``3.13``, ``3.15``, ``3.17``).

What's Included in Each Release
-------------------------------

The modules that are released as part of each GNOME version are defined in the `gnome-build-meta <https://gitlab.gnome.org/GNOME/gnome-build-meta/>`_. They include the:

* libraries used by other components
* components that make up the GNOME system, including gnome-shell
* the core GNOME apps, such as Settings and Files
* GNOME's developer apps, including Builder 

The process of adding and removing apps from the core set happens in the `app organization project <https://gitlab.gnome.org/Teams/Releng/AppOrganization/>`_.


.. toctree::
   :hidden:

   release-planning/freezes
   release-planning/release-team
