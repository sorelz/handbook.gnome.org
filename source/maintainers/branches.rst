Branches
========

In addition to the ``main`` or ``master`` branch (the default branch when you do a simple Git clone), GNOME also has stable branches corresponding to GNOME major versions, which are used for stable releases. Projects that follow GNOME versioning should use the format “gnome-VERSION” for stable branch names. For example, a stable branch targeting the GNOME 45 release should be named ``gnome-45``. Projects that do not follow GNOME versioning may replace the word “gnome” with the project name (e.g. ``gtk-4-12`` or ``libsoup-3-2``). Do not use different naming schemes; for example, do not use underscores or include the word “branch” in the name of the branch. If the stable branch does not start with “gnome-” then the branch rules must be configured in the GitLab settings to ensure the stable branches are protected, to avoid accidental damage.

Maintainers have discretion as to when to create stable branches. Some maintainers prefer to create a stable branch alongside the beta release, at the start of the :doc:`freeze period </release-planning/freezes>`, so that development can proceed unencumbered on the ``main`` branch. Other maintainers prefer to wait until after a .0 or .1 release, to reduce the need for backporting commits from ``main`` or ``master`` to the stable branch. Another strategy is to branch only when you are about to land a commit that you don't want on the new stable branch.

Example
-------

Here’s an example, when branching for 45 (inside a clone of the module!):

::

   $ git branch gnome-45 main
   $ git push -u origin gnome-45

The ``-u`` option (short for ``--set-upstream``) ensures that the local branch is properly setup to track the remote branch.

The branch doesn’t have to be based on ``main`` or ``master`` if you already started development for the next version and the latest commits shouldn’t be included. You can put any `git reference <https://git-scm.com/book/en/v2/Git-Internals-Git-References>`_ instead, such as a commit hash, or a tag.

