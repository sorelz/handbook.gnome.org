Travel
======

Please note: the Travel Committee is now accepting applications for travel funding via `GitLab <https://gitlab.gnome.org/Teams/TravelCommittee/-/issues/new?issue%5Bconfidential%5D=true>`_.

Overview
--------

One of the ways that the GNOME Foundation supports the GNOME Project is by sponsoring contributors to attend GNOME events such as conferences and hackfests. Travel sponsorship requests are processed by the :doc:`Travel Committee </foundation/committees/travel>`.

The GNOME Foundation must maintain effective control of expenses in order to maintain its financial viability and tax exempt status. The Foundation is also accountable to our donors to ensure that we manage their contributions wisely and maximize our ability to pursue our charitable mission. As such, the Foundation expects travelers to use good judgment and to claim reimbursement for only those expenses that are necessary and reasonable. 

Read the guidelines below to find out if you qualify, and follow the instructions below to request sponsorship. 

Don't panic, just ask
~~~~~~~~~~~~~~~~~~~~~

The bar required for travel sponsorships in general is not high. They are intended to maximize participation and contribution, not to make sponsorships a lofty prize only for the few. Don't let the size of this page fool you — it's an easy process. **If you're asking yourself, "Should I apply?", the answer is "YES!"**

  Note: this sponsorship policy for GNOME Project contributors is different from the `GNOME Foundation Travel and Reimbursable Expense Policy <https://wiki.gnome.org/Foundation/TravelPolicy>`_, although some sections are similar.

Which events qualify?
~~~~~~~~~~~~~~~~~~~~~

* GNOME events
* Conferences or events not affiliated with GNOME where the contributor is representing the GNOME Project

Who can be sponsored?
~~~~~~~~~~~~~~~~~~~~~

* GNOME Foundation members
* Interns working on GNOME, including Google Summer of Code or Outreachy
* Speakers (GNOME members speaking at non-GNOME events, or speakers at GNOME events who are not Foundation members)
* Non-Foundation members who are relevant to a particular GNOME event

  Note: Foundation Board Members must apply for funding using `Foundation Travel Policy <https://wiki.gnome.org/Foundation/TravelPolicy>`_, since receiving funding from the Travel Committee would constitute a conflict of interest as per the `bylaws <https://wiki.gnome.org/Foundation/Bylaws>`_, section 9.7.7.

Generally, GNOME Foundation members and speakers at GNOME conferences are given higher priority when allocating sponsorships. If you contribute to GNOME, `you can become a GNOME Foundation member too <https://foundation.gnome.org/membership/>`_!

What's covered
~~~~~~~~~~~~~~

Please refer to the policy details below for more information on what is covered. Here are the highlights:

* Travel costs: flight tickets, train tickets, travel to and from airport, etc.
* Accommodation: hostel, hotel, etc.
* Visa applications

A financial assistance package covering meals and other incidental expenses can be requested in the application form.

What's not covered
~~~~~~~~~~~~~~~~~~

* Travel and accommodation if you do not attend the event, for example due to a rejected visa application.

How to apply and what's expected of you
---------------------------------------

Before attending the event
~~~~~~~~~~~~~~~~~~~~~~~~~~

* Fill out a `GitLab issue using the application template <https://gitlab.gnome.org/Teams/TravelCommittee/-/issues/new?issue%5Bconfidential%5D=true>`_. You should receive a message from the travel committee within 72 hours letting you know they received your request.
* Wait for the Travel Committee to process your request. You may be asked to follow up with more information, or you will be sent a decision. Requests usually take a few weeks to process, so send your request in as early as possible.
* If your sponsorship request is approved, reply to the email to confirm that you accept the sponsorship and its terms. 
* Proceed with bookings within one week of accepting the sponsorship.

Do not begin booking things before your sponsorship has been approved, since we will not cover expenses that are not approved. Unless you get an explicit approval message from the travel committee and you have sent back your confirmation, your request is not yet approved and you will be personally liable for the expenses.

During the event
~~~~~~~~~~~~~~~~

* Attend the event and participate in the ways you indicated in your application.

After attending the event
~~~~~~~~~~~~~~~~~~~~~~~~~

* Write a report in the form of a blog post. Use the `round <https://gitlab.gnome.org/Teams/Websites/brand.gnome.org/-/raw/master/assets/travel-committee/sponsored-by-foundation-round.png?ref_type=heads&inline=false>`_ or `rectangular <https://gitlab.gnome.org/Teams/Websites/brand.gnome.org/-/raw/master/assets/travel-committee/sponsored-by-foundation.png?ref_type=heads&inline=false>`_ badge to show that you were sponsored by the GNOME Foundation. If you don't have your own blog you can request to add your post to `the GNOME Engagement blog <https://blogs.gnome.org/engagement/>`_ by `creating a GitLab issue <https://gitlab.gnome.org/Teams/Engagement/Social-Media-and-News>`_, and the Engagement Team can decide whether to add it in full or as part of a larger post.
* Submit a `reimbursement form <https://gitlab.gnome.org/Teams/Websites/travel-resources/-/raw/main/Reimbursement_Request_-_YOUR_NAME_-_ACTIVITY__DATE.ods?inline=false>`_ within 6 weeks of the end of the event to the Travel Committee. Provide receipts (not bank or credit card statements) for all relevant costs.

Tips
----

Searching for and booking your travel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Find the best fares for tickets and accommodation: look at various airlines and routes. Buy online and avoid agencies if at all possible, since they usually charge 10% or 20% extra.
* Use sites like `kayak.com <https://kayak.com>`_ or `flights.google.com <https://www.google.com/travel/flights>`_ to compare ticket prices. Remember to save a screenshot or PDF printout of your search so you can attach it to your sponsorship application form.
* Find a roommate to save on lodging costs, if possible. Ask if you need help, since other people are often looking for roommates. A group apartment such as one found on Airbnb is a good option if you are sharing with several people.
* Be responsible, the money you save is the money we will use to fund someone else or even yourself again.
* If you plan to stay longer, for example to do some tourism, you can book your tickets for different dates than the ones you requested in your application. However, that does not change the amount of sponsorship given.

What to do in unusual cases
~~~~~~~~~~~~~~~~~~~~~~~~~~~

* We try to respond quickly, but the travel committee runs on volunteers. If you do not receive an acknowledgement within 72 hours, please follow up with the committee, since your request may have gotten lost.
* Sometimes personal circumstances or airlines change, and travel plans can't go forward. If that happens, inform the travel committee as soon as possible and we'll figure out where to go from there. 
* If you can't submit the report or write the blog post within 6 weeks, let the travel committee know before the 6 weeks are up so that you can agree to a new deadline.
* If it would be a financial hardship to book tickets and get reimbursed after the event, contact the travel committee. Earlier reimbursements or other alternatives are possible, and are agreed on a case by case basis.
* If the airfare that you requested in your sponsorship application is no longer available when your sponsorship is approved, then you may go ahead and book an alternative if it meets certain requirements; see the airfare policy below.
* Sometimes a flight booking is required in order to submit a visa application to the country where you are traveling. If there is a risk that your visa might be denied, make sure to book a refundable flight. We don't reimburse you if you don't attend the event, so if your visa is denied you need to be able to get your flight refunded.

Invitation letters
------------------

The hosting organization or a person who is part of the local team may be able to provide an invitation letter, which may be required or recommended for applying for a visa. The conference should document the process for visa applications on their website. Should you not find any information, then please contact the organizers directly.

Reimbursement reports
---------------------

Unless agreed otherwise, you will be reimbursed after the event. Fill in the reimbursement form and upload it to your sponsorship ticket, along with scanned or photographed copies of your receipts and a link to your blog post or report.

If you are unable to write a public blog post and agreed with the travel committee to write a private report, attach it to your sponsorship ticket.

You can be reimbursed up to the amount that was approved in your sponsorship acceptance.

Make sure to send in your report within a few weeks after the event. If you are delayed, let the travel committee know so that you can agree to a new deadline. If we don't have a report within 3 months of the event, we may not be able to provide the reimbursement.

If you have any questions about how to fill out your reimbursement report, please contact the travel committee. The #travel channel on IRC is a good place to ask questions informally.

Application form
----------------

To apply for travel sponsorship, fill out the `issue template in GitLab <https://gitlab.gnome.org/Teams/TravelCommittee/-/issues/new?issue%5Bconfidential%5D=true>`_.

.. toctree::
   :hidden:

   travel/sponsorship-policy
