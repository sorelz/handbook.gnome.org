Hackfests
=========

Hackfests are events where members of the GNOME community get together to work on GNOME-related activities. This can include design and development, translation, docs, marketing, websites, leadership and strategy, and training for community members.

Find out :doc:`how to organize a hackfest </events/hackfests/organizing>` of your own.

2024
----

.. list-table::
  :widths: 50 25 25
  :header-rows: 1

  * - Name
    - Dates
    - Location

  * - `GTK Hackfest 2024 <https://wiki.gnome.org/Hackfests/GTK2024>`_
    - 1-3 February
    - Brussels, Belgium

  * - `GStreamer Spring Hackfest 2024 <https://hedgedoc.gnome.org/gstreamer-hackfest-spring-2024>`_
    - 27th–29th May 2024
    - Thessaloniki, Greece

  * - `GNOME ♥ Rust 2024 <https://hedgedoc.gnome.org/hackfest-gnome-rust-2024>`_
    - 31st of May–5th of June
    - Thessaloniki, Greece


2023
----

.. list-table::
  :widths: 50 25 25
  :header-rows: 1

  * - Name
    - Dates
    - Location
  * - `GNOME ♥ Rust 2023 <https://wiki.gnome.org/Hackfests/Rust2023>`_
    - 14–15 January
    - Online
  * - `GTK <https://wiki.gnome.org/Hackfests/GTK2023>`_
    - 1–3 February
    - Brussels, Belgium
  * - `Shell & Display Next <https://wiki.gnome.org/Hackfests/ShellDisplayNext2023>`_
    - 24–26 April
    - Brno, Czechia
  * - `GNOME Mobile <https://wiki.gnome.org/Hackfests/MobileBerlin2023>`_
    - 5–7 June
    - Berlin, Germany
  * - `GNOME 45 Hackfest & Release Party <https://wiki.gnome.org/Hackfests/Berlin45>`_
    - 15–17 September
    - Berlin, Germany
  * - `GStreamer <https://wiki.gnome.org/Hackfests/GstAutumnHackfest2023>`_
    - 27–28 September 
    - A Coruña, Spain

Previous Hackfests
------------------

See https://wiki.gnome.org/Hackfests/ for information about hackfests before 2023.

.. toctree:: 
    :hidden:

    hackfests/organizing
    hackfests/template
