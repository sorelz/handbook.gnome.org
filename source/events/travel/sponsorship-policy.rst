Travel Sponsorship Policy
=========================

This policy describes what can and can't be claimed as part of travel sponsorship, as well as guidance on pricing and booking. It is maintained by the :doc:`Travel Committee </foundation/committees/travel>`.

Lodging costs
-------------

You are expected to be cost-conscious and prudent when booking accommodations. We only sponsor room charges (plus relevant taxes and fees) for the necessary travel dates. Your receipt from the accommodations must clearly show the dates of stay.

If possible, please share accommodations with other hackfest attendees. For example, renting a shared house with a separate bedroom for each attendee can often be both cheaper and more pleasant than a hotel.

For many GNOME hackfests and conferences, the organizers will include a lodging recommendation and price range. In this case, you are eligible to receive sponsorship up to the recommended amount, even if you choose to stay elsewhere.

If there is no recommendation from the organizers, you may determine a maximum price by searching on Booking.com or an equivalent website for "Hotels" in the target city with a review score of 7.5 or higher. The maximum price is the lowest price in that list, plus 20%. In this case, please include a screenshot of your hotel search. (Note, we determine the maximum price based on hotels in the target city, but you may also book B&Bs, guesthouses, or hostels.)

We recognize that different people have different needs for their accommodations. If the above two methods of determining a price range do not yield any possible lodging that meets your needs, use your judgement to select a lodging option that does, and make a note of your requirements in your application form.

We do not reimburse any costs except the room charges. The following is a non-exhaustive list of what is not included:

* Room service
* Upgraded options to include breakfast with the room
* Mini bar purchases
* Additional services that the hotel provides, such as laundry, spa charges, or movie rentals
* Gratuities for hotel staff or other service professionals

Pre-booked accommodations
-------------------------

At some large GNOME conferences, often blocks of rooms will be pre-booked and assigned to you when your sponsorship is approved. In that case you won't have to book your own accommodations. If this is happening, it should be mentioned in the conference's call for sponsorship applications. 

If you would like to book a different accommodation to the pre-booked option, please mention this in your application. You may be eligible to receive sponsorship up to the cost of the pre-booked room, or more if the pre-booked accommodations are full.

Visa costs
----------

The Travel Committee will reimburse reasonable visa fees for standard applications. If you require a visa, please submit your sponsorship application early enough to allow plenty of time to apply for the visa. Normally we do not reimburse any expedited visa services. The travel committee may choose to reimburse an expedited visa application at its discretion, in special cases when a quicker visa application is needed.

Airfare costs
-------------

Class of service
~~~~~~~~~~~~~~~~

Coach and/or Economy Airfare is the only acceptable class for all flights (domestic and international), unless you provide a valid reason (such as a need for business class due to a documented medical reason), and the travel committee approves a special exception. You must receive written approval from the travel committee and submit a copy of it with your reimbursement request.

Low fare
~~~~~~~~

The travel committee's approval of a sponsorship request for airfare is based on the travel time compared to the flight with the lowest available fare.

To find the lowest available fare, run a flight search that meets these criteria, and save the results:

* The search must include fares from multiple airlines. Any widely-recognized airfare search site that lists results from multiple airlines is acceptable, including sites such as Orbitz, Kayak or Hipmunk
* The search must cover only the dates of relevant travel. For example, if you're attending a conference that runs Monday through Friday, the search must have you arriving no earlier than Sunday, and leaving no later than Saturday.
* The search must not use filters that might exclude the least expensive fare. For example, you may filter out flights with two or more connections, since the Foundation does not consider those reasonable. However, you may not filter out specific airlines, or flights without Wi-Fi.

Save the results of this search and attach it to your application form. A PDF printout of the first page of results from your browser is ideal. A screenshot can work too. Just make sure the output shows the search criteria and the lowest available fare.

The budget for a flight is set depending on how its cost and travel time compares to the flight with the lowest available fare. Travel time is measured from the scheduled departure time of the first flight in the itinerary to the scheduled landing time of the final flight. We use the following table to determine the budget:

.. list-table::
  :widths: 50 50
  :header-rows: 1

  * - If the travel time for a flight is...
    - the budget for that flight is...
  * - less than three hours shorter than the flight with the lowest fare
    - the lowest available fare + US$100
  * - between three and six hours shorter
    - the lowest available fare + US$200
  * - between six and ten hours shorter
    - the lowest available fare + US$350
  * - at least ten hours shorter
    - the lowest available fare + US$600

Any flight with a total cost that is within its corresponding budget, can be approved by the travel committee. Travelers may book their tickets on different dates or a different site as long as they used a qualifying fare search site to determine the amount submitted with the sponsorship request.

Reasonable flights
~~~~~~~~~~~~~~~~~~

We ask that you allow for flexibility with respect to departure times during a desired day of travel, as well as longer trips in order to reduce cost. However, we do consider flights with two or more connections as unreasonable, so you may filter them out in your flight search.

If your visa application for the country you are traveling to requires you to have already booked a flight, then you must book a refundable flight if you are at risk of your visa being denied. In this case, we consider nonrefundable flights an unreasonable burden on the traveler, as you should be able to get your flight refunded if the visa application is denied.

Allowed baggage
~~~~~~~~~~~~~~~

You are allowed one carry on and one piece of checked luggage. If an airline charges for a carry on or a single piece of checked baggage, those baggage expenses can be reimbursed. We do not reimburse baggage charges beyond those. 

Expired airfares
~~~~~~~~~~~~~~~~

Once your sponsorship is approved, you will have one week to book your travel after you confirm it. If the airfare that you requested in your sponsorship application is no longer available, then you may still book an alternative that is no more than 10% above the originally approved airfare in your sponsorship application. In this case, you must inform the travel committee and include a screenshot or PDF printout of your new flight search, like the one you attached to your application form.

If there are no options within 10% of the original airfare or more than one week has passed since the sponsorship was confirmed, then we unfortunately cannot reimburse the additional expenses without approving the new price. In that case, inform the travel committee and we'll figure out where to go from there. For this reason, make sure to book your arrangements as soon as possible after the sponsorship is approved and confirmed.
