GUADEC
======

`GUADEC <https://guadec.org>`_ is GNOME's main conference, which has been held annually since 2000. It is a contributor-focused event where members of the project get together to share what they have been working on and make plans for the future.

Where possible, facilities are provided for online participation.

Questions about GUADEC can be asked in the `GUADEC Matrix room <https://matrix.to/#/#guadec:gnome.org>`_, as well as through the `GUADEC tag on Discourse <https://discourse.gnome.org/tags/c/community/guadec>`_.

"GUADEC" Meaning
----------------

"GUADEC" originally stood for "GNOME Users and Developers European Conference". However, nowadays "GUADEC" is treated as a word in its own right, and is no longer used as an acronym.

`Since 2019 <https://foundation.gnome.org/2019/11/02/locations-for-guadec-2020-and-2021-announced/>`_, GUADEC has alternated between a European and a non-European location each year.

Organizing GUADEC
-----------------

Every GUADEC conference relies on local volunteers to help organize and run the conference. If you are interested in hosting a GUADEC conference, you can contact ``info@gnome.org`` for more information. You can also look out for the call for bids that are posted each year on the `GNOME Foundation blog <https://foundation.gnome.org/news/>`_. 

Next GUADEC
-----------

The next GUADEC is planned to happen in Denver, Colorado (USA). Details will appear at `guadec.org <https://guadec.org>`_.

Previous GUADECs
----------------

.. list-table::
  :widths: 10 10 80
  :header-rows: 1

  * - Number
    - Year
    - Location
  * - `24 <https://events.gnome.org/event/101/>`_
    - 2023
    - Riga (Latvia)
  * - `23 <https://events.gnome.org/event/77/>`_
    - 2022
    - Guadalajara (Mexico)
  * - `22 <https://events.gnome.org/event/9/>`_
    - 2021
    - Online only
  * - `21 <https://events.gnome.org/event/1/>`_
    - 2020
    - Online only
  * - `20 <https://2019.guadec.org>`_
    - 2019
    - Thessaloniki (Greece)
  * - `19 <https://2018.guadec.org>`_
    - 2018
    - Almería (Spain)
  * - `18 <https://2017.guadec.org>`_
    - 2017
    - Manchester (United Kingdom)
  * - `17 <https://2016.guadec.org>`_
    - 2016
    - Karlsruhe (Germany)
  * - `16 <https://2015.guadec.org>`_
    - 2015
    - Gothenburg (Sweden)
  * - `15 <https://2014.guadec.org>`_
    - 2014
    - Strasbourg (France)
  * - `14 <https://2013.guadec.org>`_
    - 2013
    - Brno (Czech Republic)
  * - `13 <https://2012.guadec.org>`_
    - 2012
    - A Coruña (Spain)
  * - `12 <https://desktopsummit.org>`_
    - 2011
    - Berlin (Germany)
  * - `11 <https://2010.guadec.org>`_
    - 2010
    - The Hague (The Netherlands)
  * - `10 <https://web.archive.org/web/20180429174317/http://www.grancanariadesktopsummit.org/>`_
    - 2009
    - Las Palmas (Spain)
  * - `9 <https://2008.guadec.org>`_
    - 2008
    - Istanbul (Turkey)
  * - `8 <https://2007.guadec.org>`_
    - 2007
    - Birmingham (United Kingdom)
  * - `7 <https://2006.guadec.org>`_
    - 2006
    - Vilanova (Spain)
  * - `6 <https://2005.guadec.org>`_
    - 2005
    - Stuttgart (Germany)
  * - `5 <https://2004.guadec.org>`_
    - 2004
    - Kristiansand (Norway)
  * - `4 <https://2003.guadec.org>`_
    - 2003
    - Dublin (Ireland)
  * - 3
    - 2002
    - Seville (Spain)
  * - 2
    - 2001
    - Copenhagen (Denmark)
  * - 1
    - 2000
    - Paris (France)
