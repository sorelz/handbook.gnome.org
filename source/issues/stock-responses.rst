Stock Responses
===============

The following stock responses can be used as comments when responding to issues according to the :doc:`issue review process <review>`. For ease of access, they can be added to your `personal comment templates <https://gitlab.gnome.org/-/profile/comment_templates>`_ in Gitlab.

Low quality description
~~~~~~~~~~~~~~~~~~~~~~~

::

  Thanks for taking the time to report this issue. Unfortunately, this report isn't very useful because it doesn't include a complete and easy to understand description of the issue. Please see the [issue reporting guide](https://handbook.gnome.org/issues/reporting.html) and update the issue report to include a more detailed description.

Crash report with no stack trace
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  Thanks for taking the time to report this issue. Unfortunately, without a stack trace from the crash it's very hard to know what caused it. Please see [stack traces](https://handbook.gnome.org/issues/stack-traces.html) for more information on how to provide a stack trace.

Incomplete stack trace or missing debug symbols
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  Thanks for taking the time to report this issue. Unfortunately, the stack trace is missing some elements that are needed to solve the problem, and this will make it hard for the developers to fix that crash. Please see [stack traces](https://handbook.gnome.org/issues/stack-traces.html) for more information on how to provide a stack trace. Thanks in advance!

Obsolete version
~~~~~~~~~~~~~~~~

::

  Thanks for taking the time to report this issue. Unfortunately, you are using a version that is old and is no longer supported. GNOME developers are no longer working on that version, so unfortunately there will not be any bug fixes for the version that you use. We recommend that you upgrade to a newer version of GNOME. Please feel free to reopen this bug if the problem still occurs with a newer version.

No activity in a year
~~~~~~~~~~~~~~~~~~~~~

::

  This issue was reported against a version which is not supported anymore. Developers are no longer working on this version. Please check if the issue you reported is present in a recent version of GNOME and update this report by adding a comment. Thank you again for reporting this and sorry that it could not be fixed for the version you originally used here.

  Without feedback this report will be closed after 6 weeks.

Issue isn't GNOME
~~~~~~~~~~~~~~~~~

::

  Thanks for taking the time to report this issue. Unfortunately, the software responsible for the issue is not maintained by the GNOME project. We kindly suggest that you report the issue to the correct issue tracker.

Duplicate
~~~~~~~~~

::

  Thanks for the bug issue. This particular issue has already been reported, so this report is being marked as a duplicate.

Needs information and no response
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  Closing this issue as the requested information has not been provided after more than 6 weeks. Please feel free to reopen the issue if the requested information is provided. Thanks!
