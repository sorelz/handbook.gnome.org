Issue Management
================

Having a healthy issue tracker is vital to the efficiency of a project: not only does it allow developers to find and work on the most important issues, but it is also key to encouraging new contributors to get involved.

This page provides guidelines and advice for those who are responsible for GNOME project issue trackers (primarily maintainers), with a view to help them get a grip or their issue trackers. It also provides more general advice for anyone doing issue management work in GNOME.

Basic guidelines
----------------

Make sure that the essentials of issue management are being taken care of:

* Triage all new issue reports, using the :doc:`issue review process </issues/review>`.
* Regularly conduct reviews of existing issues, focusing on types of issues which are likely to be out of date or missing information:
   * Issues with the **Needs Information** label (should be closed after six weeks).
   * Issues with the **Needs Design** label (should be closed after one year of inactivity).
   * Issues that have not seen any activity for more than a year.
* Make sure that your project uses a template for new issues. (You can use the :doc:`standard template </issues/template>` as a starting point.)
* In general, avoid having open-ended discussions in the issue tracker, such as discussing whether a feature is needed. However, if there are occasions when you do want to use an issue to have a discussion, clearly flag it as such using the **RFC** label. It is recommended to close such issues after two weeks of inactivity.

Feature request policies
------------------------

Feature requests submitted through issue trackers can be difficult to resolve and can become adversarial. It can therefore be beneficial for modules to not accept feature requests through their issue trackers, and to have alternative arrangements for new feature suggestions. If a module's maintainers decide to adopt this policy:

* it should be clearly documented (in the project's issue template and/or README)
* an alternative forum should be provided for feature discussions, such as a Discourse tag and/or Matrix chat room
* the location of the alternative forum should be advertised in the project description in Gitlab as well as in the project new issue template
* maintainers should ideally engage with feature suggestions and not simply ignore them; however, they shouldn't feel obliged to approve or reject each and every proposal

Issues can still be used to track new features when a no feature requests policy is in place. However, this should only be done after a new feature has been discussed and there is a consensus on what is wanted.

Set a good example
------------------

If established contributors don't follow our issue guidelines, then we cannot expect external participants to do the same. It is therefore important to lead by example:

* File issues for bugs before opening an MR that contains the fix.
* When filing issues yourself, fill out the issue template and follow the :doc:`issue reporting guidelines </issues/reporting>`.
* Discuss feature additions and changes using an appropriate forum (Matrix/Discourse/GitLab) and get rough consensus before opening an MR.

Be polite & empathetic
----------------------

The issue tracker is one of the main ways that the GNOME project interfaces with the outside world. Therefore, while there is a need to be efficient in our interactions within the issue tracker, it is also necessary to be considerate in our communications.

It is important to remember that everyone who reports an issue is a human being. These people have usually encountered a problem, which they are trying to resolve in the best way they know how. It is also important to recognize that the people who report issues today include the contributors of tomorrow.

The following guidelines are recommended when communicating via the issue tracker:

* Always be polite. In particular, always thank someone for taking the time to report an issue (this is especially true when closing it). It takes a lot of effort for an inexperienced contributor to file an issue, so by the time you read it they may have already invested significantly in writing it.
* Apologize if it hasn't been possible to resolve an issue, or if it hasn't received attention. For example, you can say: "I'm sorry that this issue has not received a response until now." or "Sorry that you've had this problem."
* If you disagree with the other person's view, it is a good idea to show that you still recognize their personal experience. "I'm sorry you had that experience" or "I can see how that would be frustrating" are good phrases to use when you disagree with a reporter.
* When closing an issue:
   * Always provide an explanation. This explanation doesn't have to be long or involved, but it should exist. For example, a simple "Sorry you've experienced this issue. Unfortunately, it isn't practical to resolve it with the current architecture." will often be enough.
   * Don't be afraid to suggest that the issue could be reopened in the future. This helps to avoid confrontation and can be useful in ambiguous cases. For example, "Closing as this doesn't seem to be reproducible; we can re-open if there's new information" can go a long way.

The :doc:`stock responses </issues/stock-responses>` are a good guide for the kind of tone to use on the issue tracker.

Encourage contributors
----------------------

Having contributors do issue reviews makes life much easier for maintainers, and maintainers are strongly encouraged to help themselves by encouraging contributions via issue reviews:

* Add issue review to your project's contribution guide, with a link to the :doc:`review guide </issues/review>`.
* Be on the lookout for potential issue review contributors. If someone is filing issues, it's a good opportunity to ask if they'd be interested in helping out with reviews.
* Giving out additional issue tracker permissions is a great way to motivate a new contributor, so don't be afraid to do it. You can always tell the contributor what they should and shouldn't do, and if you are watching the issue tracker then it's easy to intervene if necessary
* Once a new contributor has gained some :ref:`experience with basic tasks <contributing issue review>`, encourage them to take on more responsibility.

Encouraging contributors to help with the issue tracker can sometimes feel scary, but it doesn't need to be. The risks are relatively low, and the rewards can be huge.
