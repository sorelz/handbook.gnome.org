New Issue Template
==================

Help is wanted to create a standard issue template for this page. See `tracking issue <https://gitlab.gnome.org/Teams/Websites/handbook.gnome.org/-/issues/16>`_.
