Get in Touch
============

This page provides information about the communication platforms used by the GNOME project. Please read the `code of conduct <https://conduct.gnome.org/>`_ before making use of these facilities.

Matrix
------

GNOME uses Matrix for real time chat, and this is one of the main ways that participants communicate. The GNOME Matrix instance is located at `gnome.element.io <https://gnome.element.io/>`_, and can be accessed through a browser or a Matrix app.

See :doc:`Matrix </get-in-touch/matrix>` for more information.

Discourse
---------

`discourse.gnome.org <https://discourse.gnome.org/>`_ is GNOME's community discussion forum. It is where asynchronous long-form communication happens, and includes categories for technical questions, community discussion, and announcements.

See :doc:`Discourse </get-in-touch/discourse>` for more information.

Report an Issue
---------------

Issues with GNOME's software can be reported against the relevant component in `gitlab.gnome.org <https://gitlab.gnome.org/>`_. Please follow the :doc:`issue reporting guidelines </issues/reporting>` when doing this.

.. toctree::
   :hidden:

   get-in-touch/matrix
   get-in-touch/discourse
