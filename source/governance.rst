Governance
==========

This page provides information about how decisions are made in the GNOME project. It is intended as a guide for those who are new to GNOME and want to know how the project works, as well as a reference for who to contact with questions and issues.

How Decisions Are Made
----------------------

Most decisions in GNOME are made informally, by individuals working in collaboration with one another. This informal way of making decisions applies to who works on what, which features are supported, and which technical and user experience designs are implemented.

Every technical and design change in GNOME has to be made with the approval of the maintainers of the affected components (see :ref:`maintainers <maintainers>`). However, the maintainer's role is primarily as a gatekeeper, refusing proposals in a small fraction of cases, and accepting the vast majority of changes which have been developed in collaboratively within the community.

Some individuals and teams in GNOME play a significant role in its informal decision making practices. This is particularly true of the design team, which plays a key role in user experience design decisions.

General Guidelines
~~~~~~~~~~~~~~~~~~

The following are some informal rules that may be useful when approaching the GNOME project:

* If the required changes are relatively small and uncontroversial, it is not necessary to ask for permission to work on an issue. If there is an open issue that you want to fix, then just go ahead: write a fix and :doc:`propose the change </development/change-submission>`.
* For larger changes or changes which might be controversial, it is usually best to discuss the change with the relevant maintainers before starting work. This can happen by filing an issue. However, you may also need to reach out on Matrix.
* The issue tracker is the main place where technical decisions are made and recorded. Therefore, if you want to make a change, it is always a good idea to search for relevant issues (both open and closed).
* When making user experience design changes, it is usually a good idea to make contact with the design team.

Governance Structures
---------------------

While most of GNOME's decision making is informal, some roles and teams do have formal decision making powers. These include the following:

.. _maintainers:

Maintainers
~~~~~~~~~~~

GNOME's technical components are typically referred to as modules. Modules can be libraries, system services, core desktop components, or apps. Each module has one or more maintainers, who are listed in the module's ``.doap`` file (`an example <https://gitlab.gnome.org/GNOME/gnome-shell/-/blob/main/gnome-shell.doap?ref_type=heads>`_).

As the name implies, maintainers are responsible for ensuring that their modules are properly maintained. This includes making sure that bugs get fixed, issue reports are responded to, and releases are made. Maintainers have the final say over which changes can be made to their modules.

See the :doc:`maintainers page <maintainers>` for more information on this role.

Release Team
~~~~~~~~~~~~

The :doc:`Release Team </release-planning/release-team>` is responsible for making each GNOME release and for organizing the development schedule. It decides which modules are part of each GNOME release, and therefore which modules can be considered to be part of GNOME.

Formally, the Release Team is a committee of the GNOME Foundation Board of Directors. In practice, it determines its own membership.

While the Release Team does not routinely weigh in on individual technical decisions, it is the closest thing that GNOME has to a technical governing body.

GNOME Foundation
~~~~~~~~~~~~~~~~

The `GNOME Foundation <https://foundation.gnome.org/>`_ is the GNOME project's legal entity (to be specific, it is a 501c(3) non-profit, registered in California, USA). The Foundation owns GNOME's trademarks, manages the project's finances, and employs staff for critical project functions like infrastructure and events.

As the owner of the GNOME trademarks, the Foundation has authority over what software can be called GNOME, which it sets out in its `official GNOME software policy <https://wiki.gnome.org/Foundation/SoftwarePolicy>`_.

The GNOME Foundation is governed according to its `bylaws <https://wiki.gnome.org/Foundation/Bylaws>`_.

Roles within the foundation include:

* Members: the GNOME Foundation is a membership-based organization, and GNOME contributors are `eligible to become members <https://foundation.gnome.org/membership/>`_.
* Executive Director: is in charge of the GNOME Foundation and its staff.
* Board of Directors: sets the Foundation's overall direction, as well as overseeing its operations. It is elected by the GNOME Foundation membership.
* Committees: these have powers that are delegated to them by the Board of Directors, and some of them are relevant to the day-to-day activities of the wider project. Committees are often open to volunteers and contributors.
* Advisory Board: a group of supporting organizations which meets regularly with the GNOME Foundation.
