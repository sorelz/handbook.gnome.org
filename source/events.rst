Events
======

Physical events play an important role for GNOME, and the project organizes a variety of events throughout the year.

GNOME Events
------------

The main events that are organized by the GNOME project are:

* :doc:`GUADEC </events/guadec>`, our main annual conference
* :doc:`GNOME.Asia Summit </events/gnome.asia>`, our annual conference in Asia
* :doc:`Hackfests </events/hackfests>`, informal working events for smaller groups

Over its long history, other GNOME events have come and gone. Details about these can be found `on the wiki <https://wiki.gnome.org/Events>`_.

Other Events
------------

In addition to organizing its own events, the GNOME project also has a regular presence at a number of open source conferences. These include:

* `FOSDEM <https://fosdem.org/>`_
* `Linux App Summit (LAS) <https://linuxappsummit.org/>`_

Travel Sponsorship
------------------

GNOME Foundation members are able to :doc:`apply for sponsorship </events/travel>` to help with the cost of GNOME-related travel.

.. toctree::
   :hidden:

   events/guadec
   events/gnome.asia
   events/hackfests
   events/travel
