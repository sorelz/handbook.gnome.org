Infrastructure
==============

GNOME hosts a range of services on its servers. These services provide the essential infrastructure that is required for GNOME development to take place, and are made available to contributors to use as part of their work on the project.

GNOME infrastructure is maintained by the `GNOME Infrastructure Team <https://gitlab.gnome.org/Infrastructure/Infrastructure/>`_ and is supported by the GNOME Foundation. Volunteers and assistance are welcome.

Available services
------------------

This table lists the main GNOME-hosted services that may be of interest to GNOME contributors.

.. list-table::
  :widths: 30 60 5 5
  :header-rows: 1

  * - Service
    - Role
    - Requires GNOME account
    - Requires Foundation membership
  * - `gitlab.gnome.org <https://gitlab.gnome.org/>`_
    - General development platform
    - 
    - 
  * - `discourse.gnome.org <https://discourse.gnome.org/>`_
    - Discussion forum
    - 
    - 
  * - `cloud.gnome.org <https://cloud.gnome.org/>`_
    - Online file storage, calendaring, contacts, and more
    - ✔️
    -
  * - `hedgedoc.gnome.org <https://hedgedoc.gnome.org/>`_
    - Collaborative notes
    - ✔️
    - 
  * - `meet.gnome.org <https://meet.gnome.org/>`_
    - Video conferencing
    - ✔️
    - 
  * - :doc:`Email aliases </infrastructure/email-aliases>`
    -  gnome.org email addresses
    - ✔️
    - ✔️
  * - `blogs.gnome.org <https://blogs.gnome.org/>`_
    - Blog hosting
    - ✔️
    - ✔️

GNOME Infrastructure is also responsible for other operations, including the GNOME websites, GNOME store, and GNOME accounts.

Some GNOME services are not hosted by GNOME itself. The main example of this is the GNOME Matrix domain, located at `gnome.element.io <https://app.element.io/>`_.

Issues and support
------------------

Planned downtime and maintenance work are announced on Discourse. `status.gnome.org <https://status.gnome.org/>`_ can also be used to check on the status of GNOME infrastructure.

For problems with GNOME infrastructure, see the `Infrastructure Team contact page <https://wiki.gnome.org/Infrastructure/Contact>`_.

.. toctree::
   :hidden:

   infrastructure/developer-access
   infrastructure/accounts
   infrastructure/email-aliases
   infrastructure/blog-hosting
   infrastructure/gitlab
   infrastructure/ssh-keys
