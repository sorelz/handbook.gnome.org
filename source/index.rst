.. image:: img/handbook.svg

GNOME Project Handbook
======================

This site contains all the information needed to participate in the GNOME project. It is intended for both new and existing GNOME contributors.

If you are new to GNOME, the handbook is a great place to learn about how GNOME works, including what each of the teams does, how to access the GNOME development platforms, and how to communicate with other project members.

If you are already contributing to GNOME, then this handbook is where you can find essential reference information, such as the development schedule, details of events, and how to apply for travel sponsorship.

Other documentation
-------------------

The GNOME handbook provides project-level contributor documentation. Other types of documentation are available elsewhere:

* `apps.gnome.org <https://apps.gnome.org/>`_: home pages for GNOME apps
* `welcome.gnome.org <https://welcome.gnome.org>`_: tutorials and guidance for those wanting to contribute to GNOME for the first time
* `developer.gnome.org <https://developer.gnome.org/>`_: development guides for those using the GNOME platform
* `gitlab.gnome.org <https://gitlab.gnome.org/>`_: GNOME's main development platform includes documentation for teams and technical projects

Issues and corrections
----------------------

Please help to keep this site up to date by `reporting issues <https://gitlab.gnome.org/Teams/Websites/handbook.gnome.org/-/issues>`_ and proposing improvements. See the `README <https://gitlab.gnome.org/Teams/Websites/handbook.gnome.org/-/blob/main/README.md>`_ for more information on how to contribute.

.. toctree::
   :hidden:

   get-in-touch
   news
   teams
   governance
   foundation
   infrastructure
   release-planning
   development
   testing
   issues
   maintainers
   events
