GitLab
======

GitLab is GNOME's main development platform. It provides project hosting, issue tracking, team spaces, continuous integration, and more.

This page provides user information on how GNOME's GitLab instance is organized and run. For information on how to use GitLab, see `the user documentation <https://docs.gitlab.com/ee/>`_.

User roles and permissions
--------------------------

User permissions and abilities in GitLab are assigned using a set of standard `roles <https://docs.gitlab.com/ee/user/permissions.html>`_. Outside of the GNOME Gitlab group, projects and teams can manage these roles themselves in their membership settings. Within the GNOME GitLab group, roles are instead managed through :doc:`GNOME's accounts system <accounts>`.

SSH Keys
--------

An SSH key is required to create and fork projects in GitLab, as well as pushing changes to existing projects. See :doc:`SSH keys </infrastructure/ssh-keys>` for more information.

Project hosting
---------------

GNOME's GitLab instance provides an easy to use platform for hosting and developing projects. This can be used by existing contributors or those who want to develop projects that use the GNOME platform, or which are technically relevant to GNOME.

All projects hosted on GNOME infrastructure must conform to the :doc:`hosting requirements </infrastructure/gitlab/hosting-requirements>`. See the section below for details on hosting options.

Groups and namespaces
---------------------

Projects in GitLab must have a location, often called a namespace. GNOME operates the following namespaces.

Personal namespaces
~~~~~~~~~~~~~~~~~~~

This is the easiest way to host a new personal project and can be done entirely through GitLab. Just `create a new project <https://docs.gitlab.com/ee/user/project/>`_ and use your GitLab username as the project namespace. This option is perfect for personal experiments and new projects.

.. _gnome-group:

GNOME group
~~~~~~~~~~~

The `GNOME group <https://gitlab.gnome.org/GNOME>`_ in GitLab contains projects which are part of official GNOME releases, as well as other projects which have historically been close to the GNOME project.

Projects in the GNOME group get access to additional infrastructure resources. Access and permissions within the GNOME group are managed through GNOME accounts and maintainer DOAP files, and cannot be changed within GitLab.

To request a new project in the :ref:`gnome-group`.

World group
~~~~~~~~~~~

The `World group <https://gitlab.gnome.org/World/>`_ in GitLab is the location for projects which don't belong to a single person and are not part of the GNOME group. 

If you have a personal project which is growing, or would like to move an existing project to GNOME, and would like it to be more visible and not belong to a single individual, you can request to have the project added to the World group. To do this, `open an infrastructure issue <https://gitlab.gnome.org/Infrastructure/Infrastructure/issues/new?issuable_template=default-template>`_ with details of the request.

.. toctree::
   :hidden:

   /infrastructure/gitlab/hosting-requirements
   /infrastructure/gitlab/new-gnome-project
