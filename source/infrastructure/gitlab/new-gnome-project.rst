New GNOME Projects
==================

This page describes how to create a new software project that is part of the GNOME GitLab group.

Request a new repository
------------------------

To request a new repository under the GNOME namespace, you must first have :doc:`GNOME developer access </infrastructure/developer-access>` and have uploaded an SSH key to GitLab.

To request the repository, open an `infrastructure issue <https://gitlab.gnome.org/Infrastructure/GitLab/issues/new>`_ with the following information:

* The name for the new repository. This must only contain lowercase letters, digits, and '-' (with an exception to allow uppercase for names starting with "perl-") 
* If the repository is already hosted online, a link to the current repository.

Import the project
------------------

Once the new repository has been created, you can push branches and tags to it. If you have a local repository with the tags and branches you want (use ``git tag -l`` and ``git branch`` to list your tags and branches), then you can import all branches and tags with:

::

    git remote add origin git@gitlab.gnome.org:GNOME/repository-name.git
    git push origin --all

Once this is done, check in GitLab to make sure that you are happy with the contents of your repository.
