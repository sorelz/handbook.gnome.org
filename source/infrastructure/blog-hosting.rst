Blog Hosting
============

Those who have a gnome.org, gtk.org or gimp.org email alias can host blogs on `GNOME's Wordpress instance <https://blogs.gnome.org/>`_. In most cases, this will be a :doc:`gnome.org email alias </infrastructure/email-aliases>`. These are restricted to GNOME Foundation members.

How to create a blog
--------------------

To create a blog, open and fill out the `blog sign up page <https://blogs.gnome.org/wp-signup.php>`_. When doing so:

* Use a sensible name for the blog, such as your username, nick, or a module name.
* Enter a gnome.org, gtk.org or gimp.org email alias as the email address.

Once you have filled out the form, you will receive an email with a link to the new blog and a password. This password is only for blogs.gnome.org and is not managed by your GNOME account.

Planet GNOME
------------

If you would like your blog to be syndicated on `Planet GNOME <https://planet.gnome.org>`_, see :doc:`here </news/planet-gnome>`.
