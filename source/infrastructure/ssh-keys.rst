SSH Keys
========

SSH Keys are the primary authentication mechanism for Git and are required to create new projects in GitLab, fork existing projects in GitLab, and push changes to projects hosted on GNOME infrastructure. GitLab's documentation includes instructions on `how to generate and upload an SSH key <https://docs.gitlab.com/ee/user/ssh.html>`_. Note that the SSH endpoint is located at ``ssh.gitlab.gnome.org``, not ``gitlab.gnome.org``.


SSH key guidelines
------------------

In order to keep the GNOME codebase secure, it's very important that all GNOME contributors are careful how they handle their SSH keys. The following is a list of guidelines.

* Generate keys that are reasonably long. There is daily progress towards more easily breaking cryptography, either with new techniques or powerful machines, and the only way to be safer from this progress is giving people who want to break your security more work to do. Do not generate keys shorter than 2048 bits, and consider longer ones.
* Always use a strong passphrase. If your SSH key does not need a passphrase to be unlocked, anyone who gains access to the contents of your home directory also gains access to gnome.org. Similarly, if your password is not strong, it can be easily broken. Try to make sure it is composed of numbers, upper and lower case letters and symbols, and that it is reasonably long.
* Having to type in your SSH key each time is quite annoying, which can be solved with the help of ssh-add. If you run it, you will only have to type your passphrase once for the session. Most current Linux distributions will already run your session under ssh-agent automatically.
* Take care not to disclose the private part of your key (e.g. ~/.ssh/id_rsa), and ensure it is never read and/or copied by anyone other than yourself. If such a thing happens, your key is likely to end up compromised, and you should get it replaced as soon as possible.
* Do not use SSH from an insecure machine. If a machine is running public-facing servers, it is not secure. If a machine is sitting in a public lab, it is not secure. If a machine is used by people you do not trust or who are not responsible, it is probably not secure. If a machine is administered by people you do not trust or who are not responsible, it is definitely not secure.
* In general, if possible, you should use your SSH key only from a laptop or a personal workstation used only by you. Using it from a machine shared with co-workers or family members may be acceptable in some cases with extra precautions. If the only machines you have access to do not fit these requirements, we cannot currently allow you direct access to git and login accounts on gnome.org.
* Be careful about physical security. If you walk away from your computer after you have done ssh-add, preferably run ssh-add -D, but at least make sure you have your screen locked. Do the same thing if you suspend your laptop after you've done ssh-add (if someone steals your laptop, they shouldn't be able to open it up and ssh into gnome.org).

SSH key example
---------------

An example of generating a new SSH key:

::

    $ ssh-keygen -t rsa -b 4096
    Generating public/private rsa key pair.
    Enter file in which to save the key
    (/home/user/.ssh/id_rsa): <hit return>
    Enter passphrase (empty for no passphrase): <enter passphrase>
    Enter same passphrase again: <enter passphrase again>
    Your identification has been saved in
    /home/<user>/.ssh/id_rsa.
    Your public key has been saved in
    /home/<user>/.ssh/id_rsa.pub.
    The key fingerprint is:
    46:cb:4b:24:91:2a:3a:34:32:11:1d:85:12:7f:58:ab <user>@example.com
	    
The option ``-t rsa`` selects a version 2 RSA key. Version 1 RSA keys cannot be used on gnome.org since they can only be used with the insecure version 1 SSH protocol.

The option ``-b 2048`` makes a 2048-bit long key be generated. Please do not use keys shorter than this, and consider generating even longer ones.

You can also add a passphrase to an existing key without a passphrase by using ``ssh-keygen -p``. However, only do it if you have just generated the key, or if you are absolutely sure the said key had been totally safe until then. If you cannot be completely sure, your key might have been compromised meanwhile, in which case setting a passphrase will not help, and it should be immediately replaced.
