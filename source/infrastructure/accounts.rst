Accounts
========

GNOME operates an accounts system which is used to manage access to project infrastructure, as well as abilities and permissions on that infrastructure. GNOME accounts are sometimes referred to as "LDAP" or "GNOME LDAP". Each account comes with a username and password.

GNOME accounts are used to manage:

* :doc:`Developer access </infrastructure/developer-access>` for the :ref:`gnome-group`.
* Access to services on GNOME infrastructure, like `blogs.gnome.org <https://blogs.gnome.org/>`_, `cloud.gnome.org <https://cloud.gnome.org/>`_, and `meet.gnome.org <https://meet.gnome.org>`_.
* :ref:`master.gnome.org access <master-access>` (this is used to upload releases and is for maintainers only).
* gnome.org email aliases.

A GNOME account is not required to submit changes to GNOME modules, since this can be done through :doc:`merge requests in GitLab </development/change-submission>`. For an overview of which services are available and how to get access, see :doc:`infrastructure </infrastructure>`.

How to get an account
---------------------

GNOME accounts are created when someone is given :doc:`developer access </infrastructure/developer-access>` or when they become a :doc:`Foundation member </foundation>`.

.. toctree::
   :hidden:

   /infrastructure/accounts/managing-accounts
