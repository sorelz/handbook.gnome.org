Developer Access
================

GNOME developer access grants commit access and the `GitLab developer role <https://docs.gitlab.com/ee/user/permissions.html>`_ for all modules within the :ref:`gnome-group`. Anyone with developer access can push changes, manage issues and merge requests, and create and edit wiki pages.

Developer access is the only way to get these permissions for GNOME group modules: they are not available on an individual per-module basis.

Before requesting developer access, you should have been making contributions to GNOME for an ongoing period of time, and the maintainers of at least one GNOME module should know who you are and be familiar with your work.

How to request developer access
-------------------------------

If you don't already have a GNOME account, one will be created for you when developer access is granted. To prepare for this:

#. Login to `GitLab <https://gitlab.gnome.org>`_ using one of the third-party authentication providers.
#. Go to the email setting in `your GitLab profile <https://gitlab.gnome.org/-/profile>`_, and ensure that the primary email is set to the address that you'd like to be used for your new GNOME account.
#. Your GNOME account name will be based on the full name from your GitLab profile. This should correspond to your real name, and be a combination of your first, last and possibly middle names.

To request developer access, open an `account request issue <https://gitlab.gnome.org/Infrastructure/Infrastructure/issues/new? issuable_template=account-request>`_ and edit the template (this is in JSON format):

* Replace ``${action_name}`` with ``git_access``
* Replace ``${module_name}`` with the name of the module whose maintainers will approve the request

The title of the account request issue should be "Account request". Don't worry about the issue being confidential, as automation will make sure that is taken care of for you.

Once your request has been submitted, the GNOME Accounts Automation Bot will email the relevant GNOME module maintainers and ask them to authorize developer access. Once this has happened, you will receive a notification email.

After access has been granted
-----------------------------

Please complete these steps after getting developer access:

* If a new GNOME account has been created for you, it is important to reset your GNOME Account password at `password.gnome.org <https://password.gnome.org/>`_. 
* If you are planning on committing changes to GNOME modules, make sure that you familiarize yourself with the :doc:`development documentation </development>`. 
