Freezes
=======

GNOME uses development freezes to prevent certain types of changes from being made as it approaches a stable release. These freezes help GNOME contributors to coordinate their work and to increase quality. The dates for these freezes can be found in the `development schedule <https://release.gnome.org/calendar/index.html>`_, and details about what they mean can be found below.

Do remember: if you don't manage to land a change before the freeze, it's not the end of the world. The next 6-month release phase will usually only be 2 or 3 months away. Just make sure that your changes are in a merge request in GitLab.

If you have changes that you think need to be merged while a freeze is in effect, this page includes details about how to request an exception.

"The Freeze"
------------

The Freeze period consists of three freezes and the string change announcement period (these used to be scheduled independently, but are now combined):

API/ABI freeze
~~~~~~~~~~~~~~

When the API/ABI freeze is in effect, no API or ABI changes should be made in GNOME platform libraries. For instance, no new functions, no changed function signatures or struct fields. This provides a stable development platform for the rest of the development cycle.

API freeze is not required for non-platform libraries, but is recommended.

The release team can grant exceptions to the API/ABI freeze. Two approvals are required.

Feature freeze
~~~~~~~~~~~~~~

When feature freeze is in effect, no new features should be added. This allows developers focus on stability and allows the `GNOME Documentation Project <https://wiki.gnome.org/DocumentationProject>`_ to document existing functionality. Bug fixes of existing features are still allowed.

The release team can grant exceptions to the feature freeze. Two approvals are required.

UI freeze
~~~~~~~~~

When UI freeze is in effect, no major UI changes should be made without two approvals from the release team. Small fixes do not require permission.

String change announcement period
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

String changes can be made during the period between The Freeze and string freeze. However, all string changes made during this period must be announced to `Discourse <https://discourse.gnome.org/>`_ using the ``i18n`` tag.

String freeze
-------------

When string freeze is in effect, no string changes should be made without two approvals from the Internationalization Coordination Team.

This allows translators to do their work without the original English strings changing. It also allows documentation writers to take accurate screenshots.

For an explanation of string freezes and to see what kind of changes are excluded from the freeze rules, see the  `Translation Project string freeze documentation <https://wiki.gnome.org/TranslationProject/HandlingStringFreezes>`_.

Requesting freeze exceptions
----------------------------

Freeze exceptions can be requested by opening a ticket with the relevant team(s):

* `Release team <https://gitlab.gnome.org/Teams/Releng/freeze-breaks/-/issues>`_ (please read their `freeze break guidelines <https://gitlab.gnome.org/Teams/Releng/freeze-breaks/-/blob/master/README.md>`_ before opening a ticket)
* `Internationalization Coordination Team <https://gitlab.gnome.org/Teams/Translation/Coordination/-/issues/new?issuable_template=String%20Freeze%20exception%20request>`_

If you need exceptions for both types of freezes, you must submit requests in both places.
