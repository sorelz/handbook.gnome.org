Circle Committee
================

The GNOME Circle Committee is the team behind the GNOME Circle initiative, whose goal is to support and promote great software which builds on and integrates with the GNOME desktop and platform.

The GNOME Foundation Board of Directors has granted the Circle Committee the power to determine which software projects are members of the GNOME Circle scheme. This includes:

* Maintaining the list of member projects
* Processing applications to include software projects
* Maintaining the requirements and guidelines for member projects

Members
-------

* Allan Day
* Martín Abente Lahaye 
* Sophie Herold
* Tobias Bernard
* Christopher Davis
* Sonny Piers
* Brage Fuglseth
* Gregor Niehl
* Patrik Kramolis

Contact
-------

The Circle Committee can be contacted via `their Team page on GitLab <https://gitlab.gnome.org/Teams/Circle>`_.
