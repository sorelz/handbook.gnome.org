Internship Committee
====================

The GNOME Internship Committee is a committee of the GNOME Foundation Board of Directions. It is responsible for promoting, organizing, and conducting internship programs such as `Outreachy <https://www.outreachy.org/>`_, `Google Summer of Code <https://summerofcode.withgoogle.com/>`_, and the `GNOME Internships <https://wiki.gnome.org/Internships>`_.

The GNOME Internship Committee is granted the power to apply for internship programs on behalf of the GNOME Foundation, and select mentors and interns that participate in these programs.

The GNOME Internship Committee is expected to:

* Keep the GNOME Foundation and the GNOME community informed of the status of internship programs.
* Promote GNOME's participation in internship programs.
* Recruit mentors and participants (interns).
* Organize and promote activities aiming at integrating interns with our community.
* Help fundraiser activities aiming at increasing our internship capacity.

Members
-------

* Felipe Borges
* Allan Day
* Matthias Clasen
* Sri Ramkrishna
* Pedro Sader Azevedo
* Sonny Piers
* Aryan Kaushik

Contact
-------

The Internship Committee can be contacted via `their Team page on GitLab <https://gitlab.gnome.org/Teams/Internship>`_.
