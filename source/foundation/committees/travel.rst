Travel Committee
================

The GNOME Travel Committee is responsible for processing sponsorship requests for hackfests and conferences. This includes:

* Receive the annual travel budget from the Board of Directors
* Receive and process applications
* Receive and process the receipts for travel and/or accommodation
* Instruct the Director of Operations to process the reimbursements

The committee should be contacted through the travel-committee@gnome.org ticket list.

Current committee
-----------------

* David King
* German Poo Caamano
* Guillaume Gomez
* Meg Ford
* Rosanna Yuen
* Umang Jain
