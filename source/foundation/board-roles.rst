Board Roles
===========

President
~~~~~~~~~

* Acts as the chair if no chair is assigned
* Acts as the Executive Director, if there isn't one
* Has contract signing and spending authorization powers
* Is the link between the board and the Executive Director, and acts as the ED's line manager
* Sits on the compensation, executive and finance committees

Vice-President
~~~~~~~~~~~~~~

* Stands in for the President in their absence

Chair
~~~~~

* This is an optional position
* Prepares an agenda and sends it to the board before each meeting
* Chairs each board meeting
* Sits on the governance committee

Treasurer
~~~~~~~~~

* Ensures board oversight of the Foundation's financials
* Regularly reports to the board on the Foundation's finances (cash, receivables, outstanding bills, expenditures, income)
* Sits on the compensation and finance committees

Assistant Treasurer
~~~~~~~~~~~~~~~~~~~

* Assists the treasurer

Secretary
~~~~~~~~~

* Takes minutes of board and advisory board meetings
* Publishes meeting minutes
* Takes note of actions and discussion since the preceding meeting (such as items discussed on the board mailing list) and ensures they are included in the meeting minutes.
* Strives to keep information (such as process documentation) organized.

Vice-Secretary
~~~~~~~~~~~~~~

* Acts as the secretary if they are absent, and generally helps with secretarial duties